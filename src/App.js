import React, { Component } from 'react'
import AllProducts from './component/AllProducts';
import Navbar from './component/NavBar';
import Form from './component/Form';
import { connect } from 'react-redux';
import axios from "axios"
import { Routes, Route } from 'react-router-dom';
import EditProducts from './component/EditProducts';

export class App extends Component {
  constructor(props) {
    super(props);
    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    }
    this.state = {
      status: this.API_STATES.LOADING,
      errorMessage: "",
    }

    this.URL = 'https://fakestoreapi.com/products';

  }

  fetchData = (url) => {

    this.setState({
      status: this.API_STATES.LOADING,
    }, () => {

      axios.get(url).then(data => {
        this.props.initialProducts(data.data);
        this.setState({
          status: this.API_STATES.LOADED
        })

      }).catch(error => {
        this.setState({
          status: this.API_STATES.ERROR,
          errorMessage: "An API error occurred. Please try again in a few minutes."
        })
      })
    })
  }

  componentDidMount = () => {
    this.fetchData(this.URL)
  }
  
  


  render() {
    let products = this.props.products
    return (
      <>
        <Navbar />
        {this.state.status === this.API_STATES.LOADING && <div className="loader">Loading..</div>}

        {this.state.status === this.API_STATES.ERROR &&
          <div className='error'>
            <h1>{this.state.errorMessage}</h1>
          </div>
        }
        {this.state.status === this.API_STATES.LOADED && products.length === 0 &&
          <div className='error'>
            <h1>No products available at the moment. Please try again later.</h1>
          </div>
        }
        <Routes>
          <Route path='/' element={<AllProducts products={products} />} />
          <Route path='form' element={<Form />}></Route>

          <Route path="editPage/:productId" element={<EditProducts DetailsOfproducts={this.props.products} />} />
          {/* <Route path="cartPage" element={Cart} /> */}
        </Routes>

      </>
    )
  }
}

const mapStateToProps = (state) => {

  return {
    products: state.products.list
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    initialProducts: (products) => dispatch({
      type: 'INITIAL_PRODUCTS',
      payload: products
    })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);



