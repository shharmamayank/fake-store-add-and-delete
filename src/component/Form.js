import React, { Component } from 'react'
import validator from "validator"
import { connect } from 'react-redux'
import {v4 as uuidv4} from 'uuid'

import "./Form.css"

export class Form extends Component {
    constructor(props) {
        super(props)

        this.state = {
            title: '',
            category: '',
            image: '',
            description: '',
            price: '',
            rating: '',
            titleError: '',
            categoryError: '',
            imageError: '',
            descriptionError: '',
            priceError: '',
            ratingError: ''
        }
    }

    titlechange = (event) => {
        this.setState({
            title: event.target.value
        })
    }
    urlchange = (event) => {
        this.setState({
            image: event.target.value
        })
    }
    categorychange = (event) => {
        this.setState({
            category: event.target.value
        })
    }
    descriptionchange = (event) => {
        this.setState({
            description: event.target.value
        })
    }
    pricechange = (event) => {
        this.setState({
            price: event.target.value
        })
    }
    ratechange = (event) => {
        this.setState({
            rating: event.target.value
        })
    }
    finalSubmit = (event) => {
        const { title, category, description, image, rating, price } = this.state
        if (!validator.isAlpha(title)) {
            this.setState({
                titleError: "please enter valid Title"
            })
        } else {
            this.setState({
                titleError: ''
            })
        }
        if (!validator.isAlpha(description)) {
            this.setState({
                descriptionError: "please enter valid Description"
            })
        } else {
            this.setState({
                descriptionError: ''
            })
        }
        if (!validator.isURL(image)) {
            this.setState({
                imageError: "please enter valid Image url"
            })
        } else {
            this.setState({
                imageError: ''
            })
        }
        if (!validator.isAlpha(category)) {
            this.setState({
                categoryError: "please enter valid category"
            })
        } else {
            this.setState({
                categoryError: ''
            })
        }
        if (!validator.isNumeric(price)) {
            this.setState({
                priceError: "please enter valid price"
            })
        } else {
            this.setState({
                priceError: ''
            })
        }
        if (!validator.isNumeric(rating)) {
            this.setState({
                rating: "please enter valid rating"
            })
        }
        else {
            this.setState({
                ratingError: ''
            })
        }
        event.preventDefault();
        const newProduct = {
            id : uuidv4(),
            title: this.state.title,
            category: this.state.category,
            image: this.state.image,
            description: this.state.description,
            price: this.state.price,
            rating: {
                rate: this.state.rating,
                count: 0
            }
        };
        this.props.addProduct(newProduct);

    }

    render() {

        return (
            <>
                <form className='d-flex flex-column justify-content-center border ' onSubmit={this.finalSubmit}>
                    <div className="form-group border ">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" className="form-control" placeholder='Title of product' onChange={this.titlechange} />
                        <span>{this.state.titleError.length > 0 ? <label className='error-message'> {this.state.titleError} </label> : <label>&nbsp;</label>}</span>
                    </div>
                    <div className="form-group">
                        <label for="exampleInputEmail1">Image</label>
                        <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='image url' onChange={this.urlchange} />
                        <span>{this.state.imageError.length > 0 ? <label className='error-message'> {this.state.imageError} </label> : <label>&nbsp;</label>}</span>
                    </div>
                    <div className="form-group">
                        <label for="exampleInputEmail1">Category</label>
                        <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='category of product' onChange={this.categorychange} />
                        <span>{this.state.categoryError.length > 0 ? <label className='error-message'> {this.state.categoryError} </label> : <label>&nbsp;</label>}</span>
                    </div>
                    <div className="form-group">
                        <label for="exampleInputEmail1">Description</label>
                        <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='description of product' onChange={this.descriptionchange} />
                        <span>{this.state.descriptionError.length > 0 ? <label className='error-message'> {this.state.categoryError} </label> : <label>&nbsp;</label>}</span>

                    </div>
                    <div className="form-group">
                        <label for="exampleInputEmail1">Price</label>
                        <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='Price of product' onChange={this.pricechange} />
                        <span>{this.state.priceError.length > 0 ? <label className='error-message'> {this.state.priceError} </label> : <label>&nbsp;</label>}</span>
                    </div>
                    <div className="form-group">
                        <label for="exampleInputEmail1">Rating</label>
                        <input type="text" className="form-control" placeholder='Rating of product' onChange={this.ratechange} />
                        <span>{this.state.ratingError.length > 0 ? <label className='error-message'> {this.state.ratingError} </label> : <label>&nbsp;</label>}</span>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>

            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProduct: (product) => dispatch({
            type: "ADD_PRODUCT",
            payload: product
        })
    }
}

export default connect(null, mapDispatchToProps)(Form);
