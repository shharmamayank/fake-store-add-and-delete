import React, { Component } from 'react'
import "./NavBar.css"
export class Navbar extends Component {
    render() {
        return (
            <>
                <div className='Navbar-main-container'>
                    <i class="fa-solid fa-cart-shopping"></i>
                    <p>Fake Store</p></div>
            </>
        )
    }
}

export default Navbar
