import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import "./ProductDetails.css"
import { connect } from 'react-redux'
export class ProductsDetails extends Component {

    render() {
        return (
            <>
                <div className="card" style={{ width: "22rem" }} >
                    <div className='img-card mx-4 d-flex  flex-direction-column align-items-center ' style={{ height: "20rem" }}>
                        <div className='d-flex justify-content-center shadow p-3 mb-5 bg-body rounded '>
                            <img src={(this.props.data.image)} className="card-img-top align-items-baseline" style={{ width: "16rem", height: "14rem" }} alt="..." />
                        </div>
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">Title:&nbsp;{(this.props.data.title)}</h5>
                        <p className="card-text">{(this.props.data.description)}</p>
                    </div>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item">Price:{(this.props.data.price)}</li>
                        <li className="list-group-item">Count: {(this.props.data.rating.count)}</li>
                        <li className="list-group-item">Category :{(this.props.data.category
                        )}</li>
                    </ul>
                    <div className="card-body d-flex ">
                        <Link href="#" className="card-link text-decoration-none " onClick={() => this.props.deleteProduct(this.props.data.id)}>Delete</Link>
                        <Link to={`editPage/${this.props.data.id}`} href="#" className="card-link mx-3 text-decoration-none">Update</Link>
                        <Link to={"cart"} className="card-link mx-2 text-decoration-none">Cart</Link>
                    </div>
                </div>


            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteProduct: (productId) => dispatch({
            type: 'DELETE_PRODUCT',
            payload: productId
        })
    }
}

export default connect(null, mapDispatchToProps)(ProductsDetails)
