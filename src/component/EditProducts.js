import React, { Component } from 'react'
import { EDIT_PRODUCT } from '../redux/actionTypes';
import { connect } from 'react-redux';

export class EditProducts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            product: null,
            flag: false,
        }
    }
    getProduct = (id) => {
        // console.log(this.props.DetailsOfproducts.product)
        const productsData = this.props.DetailsOfproducts.find(
            (currentProduct) => {
                return currentProduct.id.toString() === id.toString();
            }
        );

        this.setState({
            ...this.state.product,
            product: productsData
        });
    }


    componentDidMount() {
        const id = window.location.href.split("/").slice(4);
        this.getProduct(id);
    }

    handleUpdateChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState((lastState) => ({
            product: { ...lastState.product, [name]: value },
        }));
    };
    editHandleSubmit = (event) => {
        event.preventDefault();
        this.props.editProduct(this.state.product);
        this.setState((lastState) => ({
            Flag: { ...lastState, Flag: true },
        }));
    };


    render() {
        return this.state.product === null || this.state.product === undefined ? (
            <h1 className="edit-message">Go to home page</h1>
        ) :
            (
                <>
                    <div className='container'>
                        <form className='d-flex flex-column justify-content-center' onSubmit={this.editHandleSubmit}>
                            <div className='main-container-form'>
                                <div className='form-group '>
                                    <label>Title </label>
                                    <input type='text' className='form-control' placeholder='Title of product' name="title"
                                        value={this.state.product.title}
                                        onChange={(event) => this.handleUpdateChange(event)}
                                        required
                                    />


                                </div>
                                <div className='form-group '>
                                    <label>Image </label>
                                    <input type='text' className='form-control' name="image"
                                        placeholder="Enter image URL"
                                        value={this.state.product.image}
                                        required
                                        onChange={this.handleUpdateChange}
                                    />

                                </div>
                                <div className='form-group '>
                                    <label>Category </label>
                                    <input type='text' className='form-control' name="category"
                                        placeholder="Enter category"
                                        value={this.state.product.category}
                                        required
                                        onChange={this.handleUpdateChange}
                                    />

                                </div>
                                <div className='form-group '>
                                    <label>Description </label>
                                    <input type='text' className='form-control' placeholder='description of product' onChange={this.descriptionchange} />

                                </div>
                                <div className='form-group '>
                                    <label> price</label>
                                    <input type='text' className='form-control' name="price"
                                        placeholder="Enter price"
                                        value={this.state.product.price}
                                        required
                                        onChange={this.handleUpdateChange}
                                    />

                                </div>
                                <div className='form-group'>
                                    <label> Rating</label>
                                    <input type='text' className='form-control' placeholder='Rating of product' onChange={this.ratechange} />

                                </div>

                                <button type='submit' className='btn btn-primary mt-2'> submit </button>
                            </div>

                        </form>

                    </div>
                </>
            )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        editProduct: (product) => dispatch({
            type: "EDIT_PRODUCT",
            payload: product
        })
    }
}

export default connect(null, mapDispatchToProps)(EditProducts);
