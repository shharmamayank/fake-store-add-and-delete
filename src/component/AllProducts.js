import React, { Component } from 'react'
import ProductsDetails from './ProductDetails'
import "./AllProducts.css"
import { Link } from 'react-router-dom'
export class AllProducts extends Component {
    render() {
        let alldata = this.props.products.map(data => {
            return <ProductsDetails key={data.id} data={data} />
        })
        return (
            <div>
                <div className='btn-container'> <Link to={"form"}> <button className='btn'>Add products</button></Link></div>
              
                {<div className='All-data-container'>
                    {alldata}</div>}
            </div>
        )
    }
}

export default AllProducts
