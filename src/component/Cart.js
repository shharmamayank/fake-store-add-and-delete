import React, { useEffect, useState } from "react";
import CartProductTab from "./CartProduct";


export default function Cart() {
    const API_STATES = {
        LOADING: "loading",

        LOADED: "loaded",

        ERROR: "error",
    };

    const [Cart, setCart] = useState([]);

    const [Status, setStatus] = useState(API_STATES.LOADING);

    const [ErrorMessage, setErrorMessage] = useState("");

    const cartUrl = "https://fakestoreapi.com/carts/2";

    const url = "https://fakestoreapi.com/products";

    const [ProductsData, setProductsData] = useState([]);

    const fetchCart = (cartUrl) => {
        setStatus(API_STATES.LOADING);

        fetch(cartUrl)
            .then((response) => response.json())
            .then((data) => {
                setCart(data);
                setStatus(API_STATES.LOADED);
            })
            .catch((error) => {
                setErrorMessage(
                    "An API error occurred. Please try again in a few minutes."
                );
            });
    };

    const FetchData = (url) => {
        setStatus(API_STATES.LOADING);

        fetch(url)
            .then((response) => response.json())
            .then((data) => {
                setProductsData(data);
                setStatus(API_STATES.LOADED);
            })
            .catch((error) => {
                setErrorMessage(
                    "An API error occurred. Please try again in a few minutes."
                );
                setStatus(API_STATES.ERROR);
            });
    };

    useEffect(() => {
        fetchCart(cartUrl);

        FetchData(url);
    }, []);
    let view = [];

    let total = 0;

    if (Cart.length !== 0 && Status === API_STATES.LOADED) {
        const cartProducts = Cart.products;

        let cartObject = {};
        cartProducts.map((product) => {
            if (cartObject[product.productId] === undefined) {
                cartObject[product.productId] = product.quantity;
            }
        });

        const cartids = cartProducts.map((product) => {
            return product.productId;
        });

        const tab = ProductsData.filter((product) => {
            if (cartids.includes(product.id)) {
                product["quantity"] = cartObject[product.id];
                return product;
            }
        });

        view = tab.map((product) => {
            total += product.price * product.quantity;
            return <CartProductTab key={product.id} data={product} />;
        });
    }

    return (
        <div className="cartItems">
            <h1>Cart Items</h1>
            {view}
            <div className="cartTotal">
                <h2>Cart Total: {total}</h2>
            </div>
        </div>
    );
}
